/* This program uses Regex matching and WebClient requests to download information from XML/HTML webpage response. */



using System;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MAC,DMSUSER,DMSPASS");
            foreach (string s in args)
            {
                string macinput = s;
                //Raw Data
                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] raw = wc.DownloadData("http://192.168.1.1/configs/" + s + "-provisioning.config?testscript=1");
                string webData = System.Text.Encoding.UTF8.GetString(raw);
                string password = "none";
                string user = "none";
                const string MatchPhonePattern = "\\\"(\\w{16})\\\"";
                Regex rx = new Regex(MatchPhonePattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                // Find matches.
                MatchCollection matches = rx.Matches(webData);
                // Report the number of matches found.
                int noOfMatches = matches.Count;
                // Report on each match.
                int iter = 0;
                foreach (Match match in matches)
                {
                    iter++;
                    if (iter == 1)
                    {
                        password = match.Value.ToString();
                        password = password.Replace(@"""", "");
                    }
                    if (iter == 2)
                    {
                        user = match.Value.ToString();
                        user = user.Replace(@"""", "");
                    }
                }
                user = user.Replace(@"""", "");
                password = password.Replace(@"""","");
                Console.WriteLine(s + "," + user + "," + password);
            }
            return;
        }
    }