import socket, requests, re, hashlib, random, math, time, os
from lxml import etree

BUFFER_SIZE = 1024
io_file = os.getlogin() + "-" + str(time.time()).replace(".","") + ".xml"
print("--Input and Output will be saved to " + io_file + "--")
cluster = ["172.30.62.31", "172.30.62.32", "172.20.39.33", "172.20.39.34"]

class BroadsoftSession():
    def cmd(self, XML):
        textBuffer = ""
        self.socket.send(str.encode(self.BSHead.replace("$command", XML)))
        while "/BroadsoftDocument" not in textBuffer:
            textBuffer += self.socket.recv(BUFFER_SIZE).decode("UTF-8")
        return textBuffer
    
    def __init__(self, Cluster):
        self.md5 = hashlib.md5()
        self.sessionId = str(random.random())[::-1].replace(".0","")
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Connecting")
        self.socket.connect((cluster[Cluster-1], 2208))
        self.BSHead = """<?xml version="1.0" encoding="UTF-8"?><BroadsoftDocument protocol = "OCI" xmlns="C" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><sessionId xmlns="">"""+self.sessionId+"""</sessionId>$command</BroadsoftDocument>"""
        print("Authenticating...")
        AuthenticationResponse = self.cmd("""<command xsi:type="AuthenticationRequest" xmlns=""><userId><USERNAME GOES HERE></userId></command>""")
        nonce = re.findall("<nonce>(.+)</nonce>", AuthenticationResponse)
        self.md5.update(str.encode(nonce[0]+":"+hashlib.new("sha1",b"<PASSWORD GOES HERE>").hexdigest()))
        print("Logging in...")
        LoginResponse = self.cmd("""<command xsi:type="LoginRequest14sp4" xmlns=""><userId>SOFTWARE_ENGINEERING</userId><signedPassword>"""+self.md5.hexdigest()+"""</signedPassword></command>""")
        print("Verifying session is valid...")
        VerifySessionResponse = self.cmd("""<command xsi:type="VerifySessionIsValidRequest" xmlns=""></command>""")
        print("\n\n[Connected], cluster="+str(Cluster))
        
UserCluster = 5
while not UserCluster <= 4 and UserCluster > 0:
    try:
        UserCluster = int(input("Cluster Number >>>"))
    except:
        continue

bs = BroadsoftSession(UserCluster)

stdin = ""
def shell():
    with open(io_file, "a+") as in_out_file:
        stdin = input("[Broadsoft,"+str(UserCluster)+"] >")
        lines = []
        while "</command>" not in stdin:
            _stdin = input("... ")
            if _stdin:
                lines.append(_stdin)
            else:
                break
        cmd_ = stdin + re.sub("\n|\t","",''.join(lines))
        in_out_file.write("<input><![CDATA[" + cmd_ + "]]></input>")
        xml_string = bs.cmd(cmd_)
        in_out_file.write("<output><![CDATA[" + xml_string + "]]></output>")
        print(xml_string)
    shell()
    
shell()
