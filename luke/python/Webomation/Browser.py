import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import sys
import time
from waiting import wait

#Browser class: Browser
class Browser:
    browser = webdriver.Chrome()
    browser.implicitly_wait(10)
    def visit(self, url_to_visit):
        self.browser.get("chrome://newtab")
        return self.browser.get(url_to_visit)
    def xpath(self, x_path):return self.browser.find_element_by_xpath(x_path)
    def get_id(self, idfield):return self.browser.find_element_by_id(idfield)