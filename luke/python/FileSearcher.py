import os, re
os.chdir("C:\\Directory\\To\\Start\\Searching\\In\\")
Pattern = "String To Search in Files"
for root, dirs, files in os.walk("Z:\\"):
    for file in files:
        try:
            with open(str(root + "\\" + file)) as ifile:
                FileData = str(ifile.read())
                Matches = re.findall(Pattern, FileData)
                if len(Matches) > 0:
                    print(root + "\\" + file)
                else:
                    print("nothing found for " + str(root + "\\" + file))
                    next
        except:
            next