var n = 0;
var c = 6;
var m = 0;

function setup() {
	createCanvas(600, 600);
	angleMode(DEGREES);
	colorMode(HSB);
	background(0);
}

function draw() {
	m++;
	if (m > 256) {
		m = 0;
	}
	var a = n * 137.5;
	var r = c * sqrt(n);
	var x = r * cos(a) + width/2;
	var y = r * sin(a) + height/2;
	fill(n % m, 255, 255);
	ellipse(x, y, 8, 8);
	n++;
}